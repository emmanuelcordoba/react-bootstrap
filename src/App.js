import React, { Fragment } from 'react';
import Inicio from './components/Inicio';
import Contacto from './components/Contacto';
import Usuarios from './components/Usuarios';
import Header from './components/Header';
import Footer from './components/Footer';
import Localidades from './components/Localidades';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <Fragment>
      <Router>
        <Header />
        <Container>
          <Row>
            <Col xs={12} md={8} className="p-3">
              <main>
                <Switch>
                  <Route path="/contacto" component={Contacto} />
                  <Route path="/usuarios" component={Usuarios} />
                  <Route path="/localidades" component={Localidades} />
                  <Route path="/" component={Inicio} />
                </Switch>
              </main>
            </Col>
            <Col xs={12} md={4} className="p-3">
              <aside>
                <h3>Aside</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam cupiditate minus necessitatibus error. Deserunt quibusdam quis tempora quo dolorem unde hic culpa consequuntur soluta, doloribus incidunt inventore voluptatem, nesciunt eum?</p>
              </aside>
            </Col>
          </Row>
        </Container>

      </Router>
      <Footer />
    </Fragment>
  );
}

export default App;
