import React from 'react';
import { Form, Button } from 'react-bootstrap';

function Contacto() {
    return (
        <Form>
            <Form.Group controlId="nombre">
                <Form.Label>Nombre</Form.Label>
                <Form.Control type="text" placeholder="Nombre" name="nombre" />
            </Form.Group>
            <Form.Group controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="ejemplo@gmail.com" />
            </Form.Group>
            <Form.Group controlId="telefono">
                <Form.Label>Telefono</Form.Label>
                <Form.Control type="tel" placeholder="123 1234567" />
            </Form.Group>
            <Form.Group controlId="mensaje">
                <Form.Label>Mensaje</Form.Label>
                <Form.Control as="textarea" rows="3" />
            </Form.Group>

            <Button variant="primary" type="submit">
                Enviar
            </Button>
        </Form>
    );
}
export default Contacto;