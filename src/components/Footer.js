import React from 'react';

function Footer() {
    return(
        <footer className="text-center">
            <p>Todos los derechos reservados &copy; 2020</p>
        </footer>
    );
}

export default Footer;