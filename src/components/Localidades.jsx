import React, {useState, useEffect} from 'react';
import { Container, Row, Form, ListGroup} from 'react-bootstrap';

const Localidades = () => {

    const [provincias, setProvincias] = useState([]);
    const [localidades, setLocalidades] = useState([]);

    const consultarProvincias = async () => {
        const solicitud = await fetch('https://apis.datos.gob.ar/georef/api/provincias');
        const respuesta = await solicitud.json();
        console.log(respuesta.provincias);
        setProvincias(respuesta.provincias.sort((a,b) => a.nombre.localeCompare(b.nombre)));
    }

    const consultarLocalidades = async (provincia_id) => {
        const solicitud = await fetch(`https://apis.datos.gob.ar/georef/api/localidades?provincia=${provincia_id}&max=1000`);
        const respuesta = await solicitud.json();
        console.log(respuesta.localidades.sort((a,b) => a.nombre.localeCompare(b.nombre)));
        setLocalidades(respuesta.localidades);
    }
    
    useEffect(() => {
        consultarProvincias();        
    },[]);

    const cambiaProvincia = (e) => {
        consultarLocalidades(e.target.value);
    }    

    return(
        <Container>
            <h3>Localidades</h3>
            <Row>
                <Form.Group controlId="provincias">
                    <Form.Label>Example select</Form.Label>
                    <Form.Control as="select" onChange={cambiaProvincia}>
                        <option default>Seleccionar...</option>
                        {
                            provincias.map(provincia => <option key={provincia.id} value={provincia.id}>{provincia.nombre}</option>)
                        }
                    </Form.Control>
                </Form.Group>
            </Row>
            <Row>
                <ListGroup>
                    {
                        localidades.map( loc => <ListGroup.Item key={loc.id}>{ loc.nombre }</ListGroup.Item>)
                    }
                </ListGroup>
            </Row>
        </Container>
    );
}

export default Localidades;
