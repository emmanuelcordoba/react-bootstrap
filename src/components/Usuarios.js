import React from 'react';
import { Table } from 'react-bootstrap';

function Usuarios() {
    let usuarios = [
        { id: 1, nombre: 'Camilia Bernales', email: 'camiliabernale@gmail.com', tipo: 'Alumno' },
        { id: 2, nombre: 'Mauricio Brozoski Yaffa', email: 'mauriciobrozoski@gmail.com', tipo: 'Alumno' },
        { id: 3, nombre: 'Carlos Javier Cueva', email: 'javiercueva@gmail.com', tipo: 'Alumno' },
        { id: 4, nombre: 'Cristian Diaz', email: 'cristiandiaz@gmail.com', tipo: 'Alumno' },
        { id: 5, nombre: 'Rodrigo Noé Ferreyra', email: 'rodrigoferreyra@gmail.com', tipo: 'Alumno' },
        { id: 6, nombre: 'Ariel Medina', email: 'arielmedina@gmail.com', tipo: 'Alumno' },
        { id: 7, nombre: 'Maria Alejandra Morales', email: 'alejandramorales@gmail.com', tipo: 'Alumno' },
        { id: 8, nombre: 'Ricky Moreno', email: 'rickymoreno@gmail.com', tipo: 'Alumno' },
        { id: 9, nombre: 'Nicolas Origuela', email: 'nicolasoriguela@gmail.com', tipo: 'Alumno' },
        { id: 10, nombre: 'Daniel Francisco Perez Navarro', email: 'panchoperez@gmail.com', tipo: 'Alumno' },
        { id: 11, nombre: 'Arnaldo Enrique Vega', email: 'ratonvega@gmail.com', tipo: 'Alumno' },
        { id: 12, nombre: 'Franco Ramon Viera', email: 'francoviera@gmail.com', tipo: 'Alumno' },
        { id: 13, nombre: 'Melanie Yustos Carol', email: 'melanieyustoscarol@gmail.com', tipo: 'Alumno' },
        { id: 14, nombre: 'Pablo Daniel Cancino', email: 'pablocancino@gmail.com', tipo: 'Alumno' },
        { id: 15, nombre: 'Bruno Falcon', email: 'brunofalcon@gmail.com', tipo: 'Alumno' },
        { id: 16, nombre: 'Octavio Falcon', email: 'octaviofalcon@gmail.com', tipo: 'Alumno' },
        { id: 17, nombre: 'Joel Ferreira', email: 'joelferreira@gmail.com', tipo: 'Alumno' },
        { id: 18, nombre: 'Andrea Gallo', email: 'andreagallo@gmail.com', tipo: 'Alumno' },
        { id: 19, nombre: 'Ana Carina Gonzalez Cabrera', email: 'carinagonzalez@gmail.com', tipo: 'Alumno' },
        { id: 20, nombre: 'Patricio Hernando', email: 'patriciohernando@gmail.com', tipo: 'Alumno' },
        { id: 21, nombre: 'Dante Potolicchio', email: 'dantepotolicchio@gmail.com', tipo: 'Alumno' },
        { id: 22, nombre: 'Julio Cesar Ruiz Perez', email: 'julioruizperez@gmail.com', tipo: 'Alumno' },
        { id: 23, nombre: 'Luis Alberto Santillan', email: 'luissantillan@gmail.com', tipo: 'Alumno' },
        { id: 24, nombre: 'Claudia Alejandra Villa', email: 'claudiavilla@gmail.com', tipo: 'Alumno' },
        { id: 25, nombre: 'Emmanuel Cordoba', email: 'emmanuelc27@gmail.com', tipo: 'Tutor' },
        { id: 26, nombre: 'Juan José Alonso Olivera', email: 'jjfalonso@gmail.com', tipo: 'Tutor' }
    ];

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Tipo</th>
                </tr>
            </thead>
            <tbody>
                {
                    usuarios.map(user => {
                        return (
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td>{user.nombre}</td>
                                <td>{user.email}</td>
                                <td>{user.tipo}</td>
                            </tr>
                        )
                    })
                }


            </tbody>
        </Table>
    );
}
export default Usuarios;